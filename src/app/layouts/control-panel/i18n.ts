export const T7E_APP_LAYOUT_CONTROL_PANEL = {
  tab_settings: {
    EN: 'Settings',
    RU: 'Настройки',
  },
  tab_account: {
    EN: 'Account',
    RU: 'Аккаунт',
  },
};
