import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {AppGuardPasswordResetService} from '../../services/guard-password-reset.service';

import {AppLogoHeaderModule} from '../../components/elements/logo-header/logo-header.module';

import {AppLayoutNevisPublicComponent} from './nevis-public.component';

@NgModule({
  declarations: [
    AppLayoutNevisPublicComponent
  ],
  imports: [
    CommonModule,

    AppLogoHeaderModule,

    RouterModule.forChild([
      {
        path: '', component: AppLayoutNevisPublicComponent, children: [
          {
            path: 'id/:id',
            loadChildren: '../../../dwfe/modules/nevis/components/public/public-info/public-info.module#NevisAccountPublicInfoModule'
          },
          {
            path: 'email-confirm/:confirmKey',
            loadChildren: '../../../dwfe/modules/nevis/components/public/email-confirm/email-confirm.module#NevisEmailConfirmModule'
          },
          {
            path: 'password-reset-req',
            canLoad: [AppGuardPasswordResetService],
            loadChildren: '../../../dwfe/modules/nevis/components/public/password-reset-req/password-reset-req.module#NevisPasswordResetReqModule'
          },
          {
            path: 'password-reset/:confirmKey',
            canLoad: [AppGuardPasswordResetService],
            loadChildren: '../../../dwfe/modules/nevis/components/public/password-reset/password-reset.module#NevisPasswordResetModule'
          },
        ]
      },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppLayoutNevisPublicModule {
}
