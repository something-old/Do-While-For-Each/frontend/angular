import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule, MatDividerModule} from '@angular/material';

import {DwfeDialogSimpleModule} from '@dwfe/components/dialog/simple/dialog-simple.module';

import {AppLanguageComponent} from './language.component';

@NgModule({
  declarations: [
    AppLanguageComponent,
  ],
  imports: [
    CommonModule,

    MatButtonModule,
    MatDividerModule,

    DwfeDialogSimpleModule,
  ],
  exports: [
    AppLanguageComponent,
  ],
})
export class AppLanguageModule {
}
