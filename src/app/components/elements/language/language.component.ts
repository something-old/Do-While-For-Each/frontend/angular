import {Component, OnInit} from '@angular/core';

import {APP_LANGS} from '../../../globals';
import {AppService} from '../../../services/app.service';

import {T7E_APP_LANGUAGE} from './i18n';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class AppLanguageComponent implements OnInit {

  items: { value: string, viewValue: string }[] = APP_LANGS;

  selectedValue: string;

  showLangList = false;

  constructor(public app: AppService) {
  }

  ngOnInit() {
    this.selectedValue = this.app.lang;
  }

  openDialog() {
    setTimeout(() => {
      this.showLangList = true;
    }, 200);
  }

  select(value: string): void {
    this.selectedValue = value;
  }

  change(): void {
    if (this.isLangChanged()) {
      this.app.lang = this.selectedValue;
    }
    this.close();
  }

  close() {
    this.showLangList = false;
    if (this.isLangChanged()) {
      this.selectedValue = this.app.lang;
    }
  }

  isLangChanged(): boolean {
    return this.selectedValue !== this.app.lang;
  }

  get i18n(): any {
    return T7E_APP_LANGUAGE;
  }
}
