import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-logo-header',
  templateUrl: './logo-header.component.html',
  styleUrls: ['./logo-header.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppLogoHeaderComponent {

  @Input() showTxt = true;
}
