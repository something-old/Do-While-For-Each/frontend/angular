import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {AppLogoHeaderComponent} from './logo-header.component';

@NgModule({
  declarations: [
    AppLogoHeaderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    AppLogoHeaderComponent,
  ],
})
export class AppLogoHeaderModule {
}
