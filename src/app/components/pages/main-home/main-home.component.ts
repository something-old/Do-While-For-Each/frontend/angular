import {Component} from '@angular/core';

import {Lang} from '@app/globals';
import {AppService} from '@app/services/app.service';

@Component({
  selector: 'app-page-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: ['./main-home.component.scss']
})
export class PageMainHomeComponent {

  lang = Lang;

  constructor(public app: AppService) {
  }
}
