import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {PagePrivacyPolicyComponent} from './privacy-policy.component';

@NgModule({
  declarations: [
    PagePrivacyPolicyComponent,
  ],
  imports: [
    CommonModule,

    RouterModule.forChild([
      {path: '', component: PagePrivacyPolicyComponent}
    ])

  ],
  exports: [
    RouterModule,
  ],
})
export class PagePrivacyPolicyModule {
}
