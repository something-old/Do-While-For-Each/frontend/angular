import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '../../../services/app.service';

import {T7E_APP_PAGE_NOT_FOUND} from './i18n';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class PageNotFoundComponent implements OnInit {

  path: string;

  constructor(public route: ActivatedRoute,
              public app: AppService) {
  }

  ngOnInit() {
    this.route.url.subscribe(arr =>
      this.path = '/' + arr.map(segment => segment.path).join('/')
    );
  }

  get i18n(): any {
    return T7E_APP_PAGE_NOT_FOUND;
  }
}
