export const T7E_APP_PAGE_NOT_FOUND = {
  thats_an_error: {
    EN: 'That’s an error',
    RU: 'Это ошибка',
  },
  requested_resource: {
    EN: 'The requested resource',
    RU: 'Запрашиваемый ресурс',
  },
  was_not_found: {
    EN: 'was not found on this server.',
    RU: 'не был найден на этом сервере.',
  },
  thats_all_we_known: {
    EN: 'That’s all we know.',
    RU: 'Это все, что мы можем сообщить.',
  },
};
