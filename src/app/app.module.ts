import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

import {NevisBtnUserModule} from '@nevis/components/btn-user/btn-user.module';
import {DwfeNevisService} from '@nevis/services/nevis.service';

import {DwfeCommonService} from '@common/services/common.service';

import {DwfeStorageService} from '@storage/services/storage.service';

import {AppComponent} from './app.component';

import {PageNotFoundModule} from './components/pages/not-found/not-found.module';
import {PageNotFoundComponent} from './components/pages/not-found/not-found.component';

import {AppCopyrightModule} from './components/elements/copyright/copyright.module';
import {AppLanguageModule} from './components/elements/language/language.module';
import {AppLogoHeaderModule} from './components/elements/logo-header/logo-header.module';

import {AppService} from './services/app.service';
import {AppGuardAuthService} from './services/guard-auth.service';
import {AppGuardControlPanelService} from './services/guard-control-panel.service';
import {AppGuardPasswordResetService} from './services/guard-password-reset.service';
import {AppGuardCanDeactivate} from './services/guard-can-deactivate';

import {AppLayoutInfoComponent} from './layouts/info/info.component';
import {AppLayoutSiteComponent} from './layouts/site/site.component';

@NgModule({
  declarations: [
    AppComponent,

    AppLayoutInfoComponent,
    AppLayoutSiteComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,

    AppCopyrightModule,
    AppLanguageModule,
    AppLogoHeaderModule,

    PageNotFoundModule,

    NevisBtnUserModule,

    RouterModule.forRoot([
        {
          path: '', component: AppLayoutSiteComponent, children: [
            {
              path: '',
              pathMatch: 'full',
              loadChildren: './components/pages/main-home/main-home.module#PageMainHomeModule'
            },
          ]
        },
        {
          path: 'auth',
          canLoad: [AppGuardAuthService],
          loadChildren: './layouts/auth/auth.module#AppLayoutAuthModule'
        },
        {
          path: 'cp',
          canLoad: [AppGuardControlPanelService],
          loadChildren: './layouts/control-panel/control-panel.module#AppLayoutControlPanelModule'
        },
        {
          path: 'n',
          loadChildren: './layouts/nevis-public/nevis-public.module#AppLayoutNevisPublicModule'
        },
        {
          path: '', component: AppLayoutInfoComponent, children: [
            {
              path: 'privacy-policy',
              loadChildren: './components/pages/privacy-policy/privacy-policy.module#PagePrivacyPolicyModule'
            },
            {
              path: 'terms-of-use',
              loadChildren: './components/pages/terms-of-use/terms-of-use.module#PageTermsOfUseModule'
            },
            {path: '**', component: PageNotFoundComponent},
          ]
        },
      ]
      // , {enableTracing: true}
    )
  ],
  providers: [
    AppGuardAuthService,
    AppGuardControlPanelService,
    AppGuardPasswordResetService,
    AppGuardCanDeactivate,

    AppService,

    DwfeNevisService,
    DwfeCommonService,
    DwfeStorageService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
