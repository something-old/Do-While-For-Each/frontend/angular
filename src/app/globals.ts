export enum Lang {
  EN = 'EN',
  RU = 'RU',
}

export const APP_LANGS: { value: string, viewValue: string }[] = [
  {value: Lang.EN, viewValue: 'English'},
  {value: Lang.RU, viewValue: 'Русский'},
];

