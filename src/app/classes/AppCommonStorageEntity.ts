import {DwfeAbstractStorageEntity} from '@dwfe/classes/DwfeAbstractStorageEntity';

import {AppCommonModel} from '../models/AppCommonModel';

export class AppCommonStorageEntity extends DwfeAbstractStorageEntity implements AppCommonModel {

  _lang;
  _prevUsername;

  get lang(): string {
    return this._lang;
  }

  set lang(value: string) {
    this._lang = value;
    this.saveToStorage();
  }

  get prevUsername(): string {
    return this._prevUsername;
  }

  set prevUsername(value: string) {
    this._prevUsername = value;
    this.saveToStorage();
  }

  get storageKey(): string {
    return 'appCommonStorage';
  }

  static of(appCommon: AppCommonModel): AppCommonStorageEntity {
    const obj = new AppCommonStorageEntity();

    obj._lang = appCommon.lang;
    obj._prevUsername = appCommon.prevUsername;

    obj.saveToStorage();

    return obj;
  }

  fromStorage(params?: any): AppCommonStorageEntity {
    if (this.fillFromStorage()) {
      const parsed = this.parsed;

      this._lang = parsed._lang;
      this._prevUsername = parsed._prevUsername;

      return this;
    } else {
      return null;
    }
  }
}
