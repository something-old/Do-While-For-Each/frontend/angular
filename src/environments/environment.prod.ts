export const environment = {
  production: true,

  COMMON: {
    GOOGLE_CAPTCHA: { // https://www.google.com/recaptcha/admin
      siteKey: 'xxxxxxx'
    },
  },

  NEVIS: {
    THIRD_PARTY: {
      GOOGLE: { // https://console.developers.google.com/apis/credentials
        client_id: 'xxxxxxx'
      },
      FACEBOOK: {
        appId: 'xxxxxxx', // https://developers.facebook.com/apps/
        version: 'v3.1'            // https://developers.facebook.com/docs/apps/versions/#versioning
      },
    }
  },

  STORAGE: {
    AVATAR: {
      max_file_size_in_kb: 100,
      valid_extentions: ['jpg', 'jpeg', 'png'],
    }
  },
};
