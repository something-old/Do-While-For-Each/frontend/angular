// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  COMMON: {
    GOOGLE_CAPTCHA: { // https://www.google.com/recaptcha/admin
      siteKey: '6LeKEm4UAAAAALQvPIr-8Kgl0ls_tWKXyH2A5Y63'
    },
  },

  NEVIS: {
    THIRD_PARTY: {
      GOOGLE: { // https://console.developers.google.com/apis/credentials
        client_id: '923540484627-9cifd9onuu0kboid4v1vmf25ee1g09e0.apps.googleusercontent.com'
      },
      FACEBOOK: {
        appId: '167940340684275', // https://developers.facebook.com/apps/
        version: 'v3.1'            // https://developers.facebook.com/docs/apps/versions/#versioning
      },
    }
  },

  STORAGE: {
    AVATAR: {
      max_file_size_in_kb: 100,
      valid_extentions: ['jpg', 'jpeg', 'png'],
    }
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
