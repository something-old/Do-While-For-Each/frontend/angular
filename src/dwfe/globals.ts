import {ElementRef} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

const DWFE_EXCHANGE_ERROR_FOR_LOGOUT = [
  'invalid_client',
  'invalid_grant',
  'invalid_token',
  'unauthorized',
  'unauthorized_client',
  'unauthorized_user',
  'unsupported_grant_type'
];

export const OPT_FOR_ANONYMOUSE_REQ = {
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
};

export enum FocusOnType {
  DWFE_INPUT = 'dwfe_input',
  CUSTOM_SELECTOR = 'custom_selector',
}


export class DwfeGlobals {

  static optForJSONAuthorizedReq(accessToken: string) {
    return {
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + accessToken
      }
    };
  }

  static optForMultipartFormDataAuthorizedReq(accessToken: string) {
    return {
      headers: {
        // 'Content-Type': 'multipart/form-data',

        // By setting ‘Content-Type’: undefined, the browser sets the Content-Type to multipart/form-data for us
        // and fills in the correct boundary. Manually setting ‘Content-Type’: multipart/form-data will fail to fill
        // in the boundary parameter of the request.

        'Authorization': 'Bearer ' + accessToken
      }
    };
  }

  static getNativeElementFromRef(elementRef: ElementRef): any {
    return elementRef && elementRef.nativeElement;
  }

  static focusOn(elementRef: ElementRef, focusOnType: FocusOnType, customSelectorValue: string = '') {
    const nativeElement = DwfeGlobals.getNativeElementFromRef(elementRef);
    if (!nativeElement) {
      return;
    }

    if (focusOnType === FocusOnType.DWFE_INPUT) {
      nativeElement.querySelector('.dwfe-form-group-material input').focus();
    } else {
      nativeElement.querySelector(customSelectorValue).focus();
    }
  }

  static deepClone(sourceObj) { // https://stackoverflow.com/questions/4459928/how-to-deep-clone-in-javascript#34624648
    if (!sourceObj) {
      return sourceObj;
    }
    const cloneObj = Array.isArray(sourceObj) ? [] : {};
    Object.keys(sourceObj).forEach(key => {
      const value = sourceObj[key];
      cloneObj[key] = (typeof value === 'object') ? DwfeGlobals.deepClone(value) : value;
    });
    return cloneObj;
  }

  static randomStr(requiredStringLength,
                   prefix = '',
                   postfix = ''): string { // https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
    let result = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    for (let i = 0; i < requiredStringLength; i++) {
      result += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return prefix + result + postfix;
  }

  static formatDate(date: Date) { // https://stackoverflow.com/questions/2013255/how-to-get-year-month-day-from-a-date-object
    const yyyyD = date.getFullYear();
    const mmD = (date.getMonth() + 1);
    const ddD = date.getDate();
    let yyyy = yyyyD + '';
    let mm = mmD + '';
    let dd = ddD + '';

    if (yyyyD < 1000) {
      yyyy = '0' + yyyy;
    }
    if (yyyyD < 100) {
      yyyy = '0' + yyyy;
    }
    if (yyyyD < 10) {
      yyyy = '0' + yyyy;
    }

    if (mmD < 10) {
      mm = '0' + mm;
    }

    if (ddD < 10) {
      dd = '0' + dd;
    }

    return yyyy + '-' + mm + '-' + dd;
  }

  static dateFromObj(obj: any, prop: string): Date {
    if (obj.hasOwnProperty(prop)) {
      const value = obj[prop];
      if (value) {
        return new Date(value);
      }
    }
    return null;
  }

  static prepareReq(value: any): any {
    return JSON.stringify(value);
  }

  static isReasonForLogoutHttpErrResp(obj: HttpErrorResponse): boolean {
    if (obj.error) {
      const isOAuth2Error = obj.error.hasOwnProperty('error');
      const errorCode = isOAuth2Error ? obj.error.error : obj.error;
      return DwfeGlobals.isReasonForLogout(errorCode);
    } else {
      return false;
    }
  }

  static isReasonForLogout(errorCode: string): boolean {
    return DWFE_EXCHANGE_ERROR_FOR_LOGOUT.includes(errorCode);
  }

  static appendChildScript(src: string, defer = false, elem: Element, onLoadFn: any) {

    const tagScript = document.createElement('script');

    tagScript.src = src;
    tagScript.async = true;
    tagScript.defer = defer;
    tagScript.onload = onLoadFn;

    elem.appendChild(tagScript);
  }

  static triggerChangeDetection(): void {

    // sometimes Angular doesn't want to see a change :(
    //
    // issue: https://github.com/angular/angular/issues/24181
    // test:  https://github.com/MyTempGitForCommunity/angular6-ngIf-not-working
    //

    window.dispatchEvent(new Event('resize'));

    // also you can use:
    // constructor(appRef: ApplicationRef) {
    // }
    // ...
    // this.appRef.tick();
  }

  static regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    //
    // == https://stackoverflow.com/questions/48843538/how-to-differentiate-multiple-validators-pattern#48844043
    //
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }

  static isEscapeKey(evt): boolean {

    let result = false;

    if ('key' in evt) {
      const key = evt.key.toLowerCase();
      result = key === 'escape' || key === 'esc';
    } else if ('keyCode' in evt) {
      result = evt.keyCode === 27;
    }
    return result;
  }

  static isEnterKey(evt): boolean {

    let result = false;

    if ('key' in evt) {
      const key = evt.key.toLowerCase();
      result = key === 'enter';
    } else if ('keyCode' in evt) {
      result = evt.keyCode === 13;
    }
    return result;
  }

  static formatBytes(bytes, decimals = 0) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  static KBytesToBytes(KB) {
    return KB * 1024;
  }

  static extractFileExtention(fileName: string): string {
    return fileName.split('.').pop();
  }
}
