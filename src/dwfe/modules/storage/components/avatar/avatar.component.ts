import {AfterViewInit, Component} from '@angular/core';
import {AbstractControl} from '@angular/forms';

import {environment} from '@environment/environment';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '@nevis/services/nevis.service';

import {DwfeStorageService} from '../../services/storage.service';
import {T7E_STORAGE_AVATAR} from './i18n';

@Component({
  selector: 'storage-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class DwfeStorageAvatarComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  maxFileSizeInKB = environment.STORAGE.AVATAR.max_file_size_in_kb;
  validExtentions = environment.STORAGE.AVATAR.valid_extentions;
  maxFileSizeInBytes: number;
  KBytesToBytes = DwfeGlobals.KBytesToBytes;

  file: File;

  cChosenFile: AbstractControl;

  constructor(public app: AppService,
              public nevisService: DwfeNevisService,
              public storageService: DwfeStorageService) {
    super();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.maxFileSizeInBytes = this.KBytesToBytes(this.maxFileSizeInKB);
      this.resetMessage(this.cChosenFile, ['errorMessage', 'successMessage']);
    });
  }

  setAvatarUrl(avatarUrl: string): void {
    this.nevisService
      .setAvatarUrl
      .run(this,
        DwfeGlobals.prepareReq({
          value: avatarUrl
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {

            this.nevisService.auth.setAvatarUrl(avatarUrl);

            this.errorMessage = '';
            this.successMessage = this.app.i18n('successfully_uploaded', this.i18n);
          } else {
            this.successMessage = '';
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  upload(): void {

    const formData = new FormData();
    formData.append('file', this.file);

    this.storageService
      .uploadByUserNevis //                  <------------- STEP 1. /upload-by-user/nevis
      .run(this,
        {
          formData: formData,
          chainExchange: true
        },
        (data: DwfeExchangeResult) => {
          if (data.result) {

            const avatarUrl = data.data.url;
            this.setAvatarUrl(avatarUrl); // <------------- STEP 2. /set-avatar-url

          } else {
            this.successMessage = '';
            this.setErrorMessage(data.description);
            this.setLocked(false); // because chainExchange
          }
        });
  }

  get i18n(): any {
    return T7E_STORAGE_AVATAR;
  }
}
