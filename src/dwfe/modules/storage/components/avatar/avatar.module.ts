import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule} from '@angular/material';

import {DwfeActionChooseFileModule} from '@dwfe/components/action/choose-file/choose-file.module';
import {DwfeActionOkModule} from '@dwfe/components/action/ok/action-ok.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';
import {DwfePreviewPicCircledModule} from '@dwfe/components/preview-pic-circled/preview-pic-circled.module';

import {DwfeStorageAvatarComponent} from './avatar.component';

@NgModule({
  declarations: [
    DwfeStorageAvatarComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,

    DwfeActionChooseFileModule,
    DwfeActionOkModule,
    DwfeOverlayModule,
    DwfePreviewPicCircledModule,
  ],
  exports: [
    DwfeStorageAvatarComponent,
  ],
})
export class DwfeStorageAvatarModule {
}
