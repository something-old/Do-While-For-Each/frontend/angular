import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {DwfeAbstractExchanger} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '@nevis/services/nevis.service';

import {UploadByUserNevisExchanger} from '../classes/StorageExchangers';
import {DWFE_STORAGE_ERROR_CODES_MAP} from '../i18n';

@Injectable({
  providedIn: 'root'
})
export class DwfeStorageService {

  exchangeOptV1 = DwfeAbstractExchanger.getExchangeOptV1(DWFE_STORAGE_ERROR_CODES_MAP);

  constructor(public http: HttpClient,
              public nevisService: DwfeNevisService) {
  }


  //
  // EXCHANGERS
  //

  getExchangeOptV2() {
    return DwfeAbstractExchanger.getExchangeOptV2(this.exchangeOptV1, this.nevisService.auth.access_token);
  }

  get uploadByUserNevis(): UploadByUserNevisExchanger {
    return new UploadByUserNevisExchanger(this.http, this.getExchangeOptV2());
  }
}
