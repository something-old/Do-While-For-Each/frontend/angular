export class NevisAccountPersonalModel {
  nickName?: string;
  nickNameNonPublic?: boolean;
  firstName?: string;
  firstNameNonPublic?: boolean;
  middleName?: string;
  middleNameNonPublic?: boolean;
  lastName?: string;
  lastNameNonPublic?: boolean;
  gender?: string;
  genderNonPublic?: boolean;
  dateOfBirth?: Date;
  dateOfBirthNonPublic?: boolean;
  country?: string;
  countryNonPublic?: boolean;
  city?: string;
  cityNonPublic?: boolean;
  company?: string;
  companyNonPublic?: boolean;
  positionHeld?: string;
  positionHeldNonPublic?: boolean;
  updatedOn?: Date;
}
