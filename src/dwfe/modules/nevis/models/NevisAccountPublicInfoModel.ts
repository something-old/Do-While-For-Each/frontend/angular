export interface NevisAccountPublicInfoModel {
  access: {
    id: number;
    avatarUrl: string;
  };
  email: {
    value?: string;
  };
  phone: {
    value?: string;
  };
  personal: {
    nickName?: string;
    firstName?: string;
    middleName?: string;
    lastName?: string;
    gender?: string;
    dateOfBirth?: string;
    country?: string;
    city?: string;
    company?: string;
    positionHeld?: string;
  };
}
