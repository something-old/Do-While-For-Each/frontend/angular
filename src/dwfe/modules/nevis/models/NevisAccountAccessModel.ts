export interface NevisAccountAccessModel {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in?: number; // for unlimited access_token 'expires_in' is missing
  scope: [];
  data: {
    id: number;
    username: string;
    authorities: string[];
    thirdParty: string;
    avatarUrl: string;
    createdOn: Date;
  };
}
