import {AfterViewInit, Component, EventEmitter, Output, ViewEncapsulation} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';

import {I18N_NEVIS_AUTH_THIRD_PARTY} from '../i18n';
import {NevisClientType} from '../../../globals';
import {DwfeNevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-auth-third-party',
  templateUrl: './third-party.component.html',
  styleUrls: ['./third-party.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAuthThirdPartyComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  @Output() isSignedIn = new EventEmitter<boolean>();

  constructor(public nevisService: DwfeNevisService,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {
    this.isSignedIn.emit(false);
  }

  performSignIn(thirdPartyData) {
    if (!thirdPartyData) {
      this.errorMessage = '';
      return;
    }

    this.nevisService
      .thirdPartyAuthExchanger
      .run(this,
        DwfeGlobals.prepareReq(thirdPartyData.req),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Login with Third-party'
            this.nevisService.performLogin(data.data, NevisClientType.TRUSTED);
            this.isSignedIn.emit(true);
          } else {
            this.setErrorMessage(data.description);
          }
          DwfeGlobals.triggerChangeDetection();
          thirdPartyData.thirdParty.performSignOut();
        }
      );
  }

  get i18n(): any {
    return I18N_NEVIS_AUTH_THIRD_PARTY;
  }
}
