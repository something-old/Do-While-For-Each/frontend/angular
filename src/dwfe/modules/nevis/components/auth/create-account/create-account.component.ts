import {AfterViewInit, Component, ElementRef, HostListener, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {Router} from '@angular/router';

import {of} from 'rxjs';
import {concatMap, delay, takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals, FocusOnType} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeCommonService} from '@common/services/common.service';

import {I18N_NEVIS_AUTH_CREATE_ACCOUNT} from '../i18n';
import {DwfeNevisService} from '../../../services/nevis.service';

@Component({
  selector: 'nevis-auth-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAuthCreateAccountComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  cTermsOfUse: AbstractControl;
  cPrivacyPolicy: AbstractControl;

  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;

  showCreatedNewAccScreen = false;

  constructor(public nevisService: DwfeNevisService,
              public commonService: DwfeCommonService,
              public router: Router,
              public app: AppService) {
    super();
  }

  ngAfterViewInit(): void {

    this
      .isCaptchaValid$
      .pipe(
        takeUntil(this.latchForUnsubscribe$),
        concatMap(x => of(x)
          .pipe(
            delay(10))) // otherwise below cEmail, cPrivacyPolicy return undefined
      ).subscribe(
      isCaptchaValid => {
        if (isCaptchaValid) {
          this.resetMessage(this.cEmail, ['errorMessage']);
          this.cTermsOfUse.setValue(false);
          this.cPrivacyPolicy.setValue(false);
          this.focusOn(this.refEmail, FocusOnType.DWFE_INPUT);
        }
      });
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value && this.refEmail) {
      this.focusOn(this.refEmail, FocusOnType.DWFE_INPUT);
    }
  }

  performCreateAccount() {

    this.nevisService
      .createAccountExchanger
      .run(this,
        DwfeGlobals.prepareReq({
          email: this.cEmail.value
        }),
        (data: DwfeExchangeResult) => {
          if (data.result) {    // actions on success 'Create account'
            this.showCreatedNewAccScreen = true;
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  goToLoginPage() {

    this.router.navigate(
      ['/auth/login'], {
        queryParams: {
          username: this.cEmail.value
        }
      });
  }

  isDisabledBtnPerformCreateAccount(): boolean {
    return !(this.cEmail && this.cEmail.valid && this.cTermsOfUse.value && this.cPrivacyPolicy.value) || this.isLocked;
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (DwfeGlobals.isEnterKey(evt)) {

      if (this.showCreatedNewAccScreen) {
        this.goToLoginPage();
      } else {
        if (!this.isDisabledBtnPerformCreateAccount()) {
          this.performCreateAccount();
        }
      }
    }
  }

  get i18n(): any {
    return I18N_NEVIS_AUTH_CREATE_ACCOUNT;
  }
}
