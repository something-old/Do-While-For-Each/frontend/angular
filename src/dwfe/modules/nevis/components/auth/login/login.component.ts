import {AfterViewInit, Component, ElementRef, HostListener, ViewChild, ViewEncapsulation} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals, FocusOnType} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '../../../services/nevis.service';
import {NevisClientType} from '../../../globals';
import {I18N_NEVIS_AUTH_LOGIN} from '../i18n';

@Component({
  selector: 'nevis-auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAuthLoginComponent extends DwfeAbstractExchangeableComponent implements AfterViewInit {

  cEmail: AbstractControl;
  cPassword: AbstractControl;
  cNotMyDevice: AbstractControl;

  @ViewChild('refEmail', {read: ElementRef}) refEmail: ElementRef;
  @ViewChild('refPassword', {read: ElementRef}) refPassword: ElementRef;
  @ViewChild('refThirdPartyGoToIndexPage', {read: ElementRef}) refThirdPartyGoToIndexPage: ElementRef;

  isThirdPartySignedIn = false;

  constructor(public app: AppService,
              public nevisService: DwfeNevisService,
              public router: Router,
              public activatedRoute: ActivatedRoute) {
    super();
  }

  ngAfterViewInit(): void {
    this.resetMessage(this.cEmail, ['errorMessage']);
    this.resetMessage(this.cPassword, ['errorMessage']);

    setTimeout(() => {

      this.cNotMyDevice.setValue(false);

      this.activatedRoute.queryParamMap.subscribe(map => {
        const username = map.get('username') || null;
        if (username) {
          this.cEmail.setValue(username);
        } else if (this.app.prevUsername) {
          this.cEmail.setValue(this.app.prevUsername);
        }
      });

      this.focus();

    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  setLocked(value: boolean): void {
    super.setLocked(value);
    if (!value) {
      this.focus();
    }
  }

  focus() {
    if (this.cEmail.invalid) {
      this.focusOn(this.refEmail, FocusOnType.DWFE_INPUT);
    } else if (this.cPassword.invalid) {
      this.focusOn(this.refPassword, FocusOnType.DWFE_INPUT);
    }
  }

  performLogin(): void {

    this.setErrorMessage('');
    this.setLocked(true);

    this.nevisService
      .setRedirectUrl('/')
      .performLoginStandard(
        this.cEmail.value,
        this.cPassword.value,
        this.cNotMyDevice.value ? NevisClientType.UNTRUSTED : NevisClientType.TRUSTED
      )
      .loginResult$
      .pipe(
        takeUntil(this.isLocked$())
      )
      .subscribe(
        (data: DwfeExchangeResult) => {
          if (data.result) { // actions on success 'Login'
          } else {
            this.setErrorMessage(data.description);
          }
          this.setLocked(false);
        }
      );
  }

  receiveThirdPartySignInResult(value) {
    this.isThirdPartySignedIn = value;
    DwfeGlobals.triggerChangeDetection();
    this.focusOn(this.refThirdPartyGoToIndexPage, FocusOnType.CUSTOM_SELECTOR, '.nevis-auth-login__third-party-btn');
  }

  isDisabledBtnPerformLogin(): boolean {
    return !(this.cEmail && this.cPassword && this.cEmail.valid && this.cPassword.valid) || this.isLocked;
  }

  goToIndexPage(): void {
    this.router.navigateByUrl('/');
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (DwfeGlobals.isEnterKey(evt)) {

      if (this.nevisService.isLoggedIn) {
        if (this.isThirdPartySignedIn) {
          this.goToIndexPage();
        }
      } else {
        if (!this.isDisabledBtnPerformLogin()) {
          this.performLogin();
        }
      }
    }
  }

  get i18n(): any {
    return I18N_NEVIS_AUTH_LOGIN;
  }
}
