import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {animate, style, transition, trigger} from '@angular/animations';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from '@app/services/app.service';

import {DwfeNevisService} from '../../../services/nevis.service';
import {T7E_NEVIS_BTN_USER_LOGGED_IN} from '../i18n';

@Component({
  selector: 'nevis-btn-user-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.scss'],
  animations: [
    trigger('animatedBlock', [  // https://angular.io/guide/animations#example-entering-and-leaving
      transition(':enter', [    // https://angular.io/api/animations/transition#using-enter-and-leave
        style({opacity: 0}),
        animate('120ms ease-in', style({opacity: 1}))
      ]),
      transition(':leave', [
        animate('120ms ease-out', style({opacity: 0}))
        // animate('150ms ease-out', style({opacity: 0, transform: 'translateX(100%)'}))
      ]),
    ])
  ],
})
export class NevisBtnUserLoggedInComponent implements OnInit, OnDestroy {

  isMenuOpen = false;

  avatarUrl: string;

  subjLatchForUnsubscribe = new Subject();

  constructor(public nevisService: DwfeNevisService,
              public router: Router,
              public app: AppService) {
  }

  ngOnInit(): void {
    this.nevisService.auth
      .avatarUrl$
      .pipe(takeUntil(this.subjLatchForUnsubscribe.asObservable()))
      .subscribe(avatarUrl => this.avatarUrl = avatarUrl);
  }

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  myAccount(): void {
    this.router.navigateByUrl('/cp');
    this.isMenuOpen = false;
  }

  clickedInside(event: Event) {
    event.preventDefault();
    event.stopPropagation();  // <- that will stop propagation on lower layers
  }

  @HostListener('document:click', ['$event'])
  clickedOutside(event) {
    this.isMenuOpen = false;
  }

  get i18n(): any {
    return T7E_NEVIS_BTN_USER_LOGGED_IN;
  }
}
