import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '@dwfe/globals';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '../../../services/nevis.service';
import {I18N_NEVIS_EMAIL_CONFIRM} from './i18n';

@Component({
  selector: 'nevis-email-confirm',
  templateUrl: './email-confirm.component.html',
  styleUrls: ['./email-confirm.component.scss']
})
export class NevisEmailConfirmComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  confirmKey: string;

  constructor(public nevisService: DwfeNevisService,
              public activatedRoute: ActivatedRoute,
              public app: AppService) {
    super();
  }

  ngOnInit(): void {
    this.confirmKey = this.activatedRoute.snapshot.paramMap.get('confirmKey');
  }

  ngAfterViewInit(): void {
    setTimeout(() => {

      if (this.nevisService.isEmailConfirmed()) {
        this.errorMessage = '';
        this.successMessage = this.app.i18n('email_already_confirmed', this.i18n);
      } else {
        this.nevisService
          .emailConfirmExchanger
          .run(this,
            DwfeGlobals.prepareReq({
              key: this.confirmKey
            }),
            (data: DwfeExchangeResult) => {
              if (data.result) {    // actions on success 'Email Confirmation'
                this.nevisService.setEmailConfirmed(true);
                this.successMessage = this.app.i18n('email_now_confirmed', this.i18n);
              } else {
                this.setErrorMessage(data.description);
              }
            });
      }
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  get i18n(): any {
    return I18N_NEVIS_EMAIL_CONFIRM;
  }
}
