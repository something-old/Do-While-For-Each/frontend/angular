import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatButtonModule} from '@angular/material';

import {DwfeActionOkModule} from '@dwfe/components/action/ok/action-ok.module';
import {DwfeFormControlInputPasswordModule} from '@dwfe/components/form-control/input-password/input-password.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';

import {NevisPasswordResetComponent} from './password-reset.component';

@NgModule({
  declarations: [
    NevisPasswordResetComponent,
  ],
  imports: [
    CommonModule,

    MatButtonModule,

    DwfeActionOkModule,
    DwfeFormControlInputPasswordModule,
    DwfeOverlayModule,

    RouterModule.forChild([
      {path: '', component: NevisPasswordResetComponent}
    ])
  ],
  exports: [
    RouterModule,
  ],
})
export class NevisPasswordResetModule {
}
