export const I18N_NEVIS_PASSWORD_RESET = {
  title_reset: {
    EN: 'Password reset',
    RU: 'Сброс пароля',
  },
  new_password: {
    EN: 'New password',
    RU: 'Новый пароль',
  },
  new_password_repeat: {
    EN: 'Repeat new password',
    RU: 'Повтор нового пароля',
  },
  different_passwords: {
    EN: 'New password and repeat do not match',
    RU: 'Новый пароль и его повтор не совпадают',
  },
  successfully_completed: {
    EN: 'Successfully completed',
    RU: 'Успешно выполнен',
  },
  go_to_login: {
    EN: 'Go to Log-in',
    RU: 'Вход',
  },
};
