import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {AppService} from '@app/services/app.service';
import {DwfeAbstractExchangeableComponent} from '@dwfe/classes/DwfeAbstractExchangeableComponent';
import {DwfeExchangeResult} from '@dwfe/classes/DwfeAbstractExchanger';

import {DwfeNevisService} from '../../../services/nevis.service';
import {NevisAccountPublicInfoModel} from '../../../models/NevisAccountPublicInfoModel';
import {NEVIS_COUNTRIES, NEVIS_GENDERS} from '../../../i18n';
import {I18N_NEVIS_ACCOUNT_PUBLIC_INFO} from './i18n';

@Component({
  selector: 'nevis-account-public-info',
  templateUrl: './public-info.component.html',
  styleUrls: ['./public-info.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NevisAccountPublicInfoComponent extends DwfeAbstractExchangeableComponent implements OnInit, AfterViewInit {

  id: string;
  publicInfo: NevisAccountPublicInfoModel;
  hiddenValue = null;

  constructor(public nevisService: DwfeNevisService,
              public app: AppService,
              public activatedRoute: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.resetPublicInfo();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.performReadFromServer();
    }); // to prevent ExpressionChangedAfterItHasBeenCheckedError
  }

  performReadFromServer() {

    this.nevisService
      .publicAccountInfoExchanger
      .run(this,
        {id: this.id},
        (data: DwfeExchangeResult) => {
          if (data.result) {
            const publicInfoResp: NevisAccountPublicInfoModel = data.data;
            if (publicInfoResp) {
              this.resetPublicInfo();
              Object.keys(this.publicInfo).forEach(key => {
                if (Object.keys(publicInfoResp[key]).length > 0) {
                  this.setIfPassed(publicInfoResp[key], this.publicInfo[key]);
                }
              });
            }
          } else {
            this.setErrorMessage(data.description);
          }
        }
      );
  }

  setIfPassed(source, target) {
    Object.keys(source).forEach(key => {
      const sourceValue = source[key];

      if (sourceValue !== null && sourceValue !== undefined) {
        if (['gender', 'country'].includes(key)) {

          if (key === 'gender' || key === 'country') {
            let arr = [];

            if (key === 'gender') {
              arr = NEVIS_GENDERS;
            } else if (key === 'country') {
              arr = NEVIS_COUNTRIES;
            }
            target[key] = this.app.i18nExtract(this.valueToViewValue(arr, sourceValue));
          }
        } else {
          target[key] = sourceValue;
        }
      }
    });
  }

  valueToViewValue(arr, value): any {
    for (const current of arr) {
      if (value === current.value) {
        return current.viewValue;
      }
    }
    return false;
  }

  resetPublicInfo(): void {
    this.publicInfo = {
      access: {
        id: this.hiddenValue,
        avatarUrl: this.hiddenValue,
      },
      email: {
        value: this.hiddenValue
      },
      phone: {
        value: this.hiddenValue
      },
      personal: {
        nickName: this.hiddenValue,
        firstName: this.hiddenValue,
        middleName: this.hiddenValue,
        lastName: this.hiddenValue,
        gender: this.hiddenValue,
        dateOfBirth: this.hiddenValue,
        country: this.hiddenValue,
        city: this.hiddenValue,
        company: this.hiddenValue,
        positionHeld: this.hiddenValue,
      }
    };
  }

  get i18n(): any {
    return I18N_NEVIS_ACCOUNT_PUBLIC_INFO;
  }
}
