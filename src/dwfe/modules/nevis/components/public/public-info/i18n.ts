export const I18N_NEVIS_ACCOUNT_PUBLIC_INFO = {
  title: {
    EN: 'Public information',
    RU: 'Открытая информация',
  },
  refresh: {
    EN: 'Refresh',
    RU: 'Обновить',
  },
  param_id: {
    EN: 'id',
    RU: 'id',
  },
  param_nickname: {
    EN: 'Nickname',
    RU: 'Никнейм',
  },
  param_email: {
    EN: 'E-mail',
    RU: 'Эл. почта',
  },
  param_phone: {
    EN: 'Phone',
    RU: 'Телефон',
  },
  param_first_name: {
    EN: 'First name',
    RU: 'Имя',
  },
  param_middle_name: {
    EN: 'Middle name',
    RU: 'Отчество',
  },
  param_last_name: {
    EN: 'Last name',
    RU: 'Фамилия',
  },
  param_gender: {
    EN: 'Gender',
    RU: 'Пол',
  },
  param_date_of_birth: {
    EN: 'Date of birth',
    RU: 'Дата рождения',
  },
  param_country: {
    EN: 'Country',
    RU: 'Страна',
  },
  param_city: {
    EN: 'City',
    RU: 'Город',
  },
  param_company: {
    EN: 'Company',
    RU: 'Компания',
  },
  param_position_held: {
    EN: 'Position held',
    RU: 'Должность',
  },
};
