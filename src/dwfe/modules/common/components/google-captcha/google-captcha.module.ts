import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RECAPTCHA_LANGUAGE, RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';

import {environment} from '@environment/environment';

import {DwfeAlertModule} from '@dwfe/components/alert/alert.module';
import {DwfeOverlayModule} from '@dwfe/components/overlay/overlay.module';

import {DwfeCommonGoogleCaptchaComponent} from './google-captcha.component';

@NgModule({
  declarations: [
    DwfeCommonGoogleCaptchaComponent,
  ],
  imports: [
    CommonModule,

    RecaptchaModule.forRoot(),

    DwfeAlertModule,
    DwfeOverlayModule,
  ],
  exports: [
    DwfeCommonGoogleCaptchaComponent,
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {...environment.COMMON.GOOGLE_CAPTCHA} as RecaptchaSettings
    },
    {
      provide: RECAPTCHA_LANGUAGE,
      useValue: 'en'
    }
  ],
})
export class DwfeCommonGoogleCaptchaModule {
}
