import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatSlideToggleModule, MatTooltipModule} from '@angular/material';

import {DwfeFormControlToggleComponent} from './toggle.component';

@NgModule({
  declarations: [
    DwfeFormControlToggleComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatTooltipModule,
  ],
  exports: [
    DwfeFormControlToggleComponent,
  ],
})
export class DwfeFormControlToggleModule {
}
