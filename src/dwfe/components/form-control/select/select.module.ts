import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatFormFieldModule, MatSelectModule} from '@angular/material';

import {DwfeFormControlClearModule} from '../clear/clear.module';

import {DwfeFormControlSelectComponent} from './select.component';

@NgModule({
  declarations: [
    DwfeFormControlSelectComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    DwfeFormControlClearModule,
  ],
  exports: [
    DwfeFormControlSelectComponent,
  ],
})
export class DwfeFormControlSelectModule {
}
