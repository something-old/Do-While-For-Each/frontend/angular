import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatDatepickerModule, MatFormFieldModule, MatInputModule, MatNativeDateModule} from '@angular/material';

import {DwfeFormControlDatePickerComponent} from './date-picker.component';

import {DwfeFormControlClearModule} from '../clear/clear.module';

@NgModule({
  declarations: [
    DwfeFormControlDatePickerComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    DwfeFormControlClearModule,
  ],
  exports: [
    DwfeFormControlDatePickerComponent,
  ],
})
export class DwfeFormControlDatePickerModule {
}
