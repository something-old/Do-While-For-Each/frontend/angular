import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatFormFieldModule, MatInputModule} from '@angular/material';

import {DwfeFormControlClearModule} from '../clear/clear.module';

import {DwfeFormControlInputTextComponent} from './input-text.component';

@NgModule({
  declarations: [
    DwfeFormControlInputTextComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    DwfeFormControlClearModule,
  ],
  exports: [
    DwfeFormControlInputTextComponent,
  ],
})
export class DwfeFormControlInputTextModule {
}
