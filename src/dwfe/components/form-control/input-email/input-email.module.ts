import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatFormFieldModule, MatInputModule} from '@angular/material';

import {DwfeFormControlClearModule} from '../clear/clear.module';
import {DwfeSpinnerDottedHorizontalModule} from '../../spinner/dotted-horizontal/dotted-horizontal.module';

import {DwfeFormControlInputEmailComponent} from './input-email.component';

@NgModule({
  declarations: [
    DwfeFormControlInputEmailComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    DwfeFormControlClearModule,
    DwfeSpinnerDottedHorizontalModule,
  ],
  exports: [
    DwfeFormControlInputEmailComponent,
  ],
})
export class DwfeFormControlInputEmailModule {
}
