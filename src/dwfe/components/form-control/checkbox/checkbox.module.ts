import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {MatCheckboxModule} from '@angular/material';

import {DwfeFormControlCheckboxComponent} from './checkbox.component';

@NgModule({
  declarations: [
    DwfeFormControlCheckboxComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    MatCheckboxModule,
  ],
  exports: [
    DwfeFormControlCheckboxComponent,
  ],
})
export class DwfeFormControlCheckboxModule {
}
