import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '../../../globals';
import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';
import {DwfeMatErrorStateMatcher} from '../../../classes/DwfeMatErrorStateMatcher';

import {T7E_DWFE_FORM_CONTROL_INPUT_PASSWORD} from './i18n';

@Component({
  selector: 'dwfe-form-control-input-password',
  templateUrl: './input-password.component.html',
})
export class DwfeFormControlInputPasswordComponent extends DwfeAbstractEditableControl implements OnInit {

  minLength = 8;
  maxLength = 55;

  validators = [
    Validators.required,
    Validators.minLength(this.minLength),
    Validators.maxLength(this.maxLength),
    DwfeGlobals.regexValidator(new RegExp('\\d'), {'atLeastOneNumber': {}}),
    DwfeGlobals.regexValidator(new RegExp('\\D'), {'atLeastOneLetter': {}}),
  ];

  compareAs = 'textField';

  hideCharacters = true;

  matcher = new DwfeMatErrorStateMatcher();

  constructor(public app: AppService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  switchHide(): void {
    this.hideCharacters = !this.hideCharacters;
  }

  get i18n(): any {
    return T7E_DWFE_FORM_CONTROL_INPUT_PASSWORD;
  }
}
