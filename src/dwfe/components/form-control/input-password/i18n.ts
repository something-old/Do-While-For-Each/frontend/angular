export const T7E_DWFE_FORM_CONTROL_INPUT_PASSWORD = {
  required: {
    EN: 'Required',
    RU: 'Обязательно',
  },
  length_err: {
    EN: 'Length must be',
    RU: 'Длина должна быть',
  },
  at_least_1_number: {
    EN: 'At least 1 number',
    RU: 'Хотя бы одна цифра',
  },
  at_least_1_letter: {
    EN: 'At least 1 letter',
    RU: 'Хотя бы одна буква',
  },
};
