import {NgModule} from '@angular/core';

import {DwfeLocationBackComponent} from './location-back.component';

@NgModule({
  declarations: [
    DwfeLocationBackComponent,
  ],
  imports: [],
  exports: [
    DwfeLocationBackComponent,
  ],
})
export class DwfeLocationBackModule {
}
