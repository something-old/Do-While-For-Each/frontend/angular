import {Component} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'dwfe-location-back',
  templateUrl: './location-back.component.html',
  styleUrls: ['./location-back.component.scss']
})
export class DwfeLocationBackComponent {

  constructor(public location: Location) {
  }
}
