import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatCardModule} from '@angular/material';

import {DwfeActionClearModule} from '../action/clear/clear.module';
import {DwfePreviewPicCircledComponent} from './preview-pic-circled.component';

@NgModule({
  declarations: [
    DwfePreviewPicCircledComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,

    DwfeActionClearModule,
  ],
  exports: [
    DwfePreviewPicCircledComponent,
  ],
})
export class DwfePreviewPicCircledModule {
}
