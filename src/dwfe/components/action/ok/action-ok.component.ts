import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'dwfe-action-ok',
  templateUrl: './action-ok.component.html',
  styleUrls: ['./action-ok.component.scss']
})
export class DwfeActionOkComponent {

  @Input() isDisabled = false;

  @Input() showErrorMessage = false;
  @Input() showSuccessMessage = false;

  @Input() errorMessage: string;
  @Input() successMessage: string;

  @Input() Label = 'Ok';

  @Input() type = 'standard';

  @Output() takeClick = new EventEmitter<boolean>();
}
