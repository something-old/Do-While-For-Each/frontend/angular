import {NgModule} from '@angular/core';

import {MatTooltipModule} from '@angular/material';

import {DwfeActionClearComponent} from './clear.component';

@NgModule({
  declarations: [
    DwfeActionClearComponent,
  ],
  imports: [
    MatTooltipModule,
  ],
  exports: [
    DwfeActionClearComponent,
  ],
})
export class DwfeActionClearModule {
}
