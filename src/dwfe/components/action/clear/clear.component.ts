import {Component, EventEmitter, Output} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {I18N_DWFE_ACTION_CLEAR} from './i18n';

@Component({
  selector: 'dwfe-action-clear',
  templateUrl: './clear.component.html',
  styleUrls: ['./clear.component.scss']
})
export class DwfeActionClearComponent {

  @Output() takeClick = new EventEmitter<boolean>();

  constructor(public app: AppService) {
  }

  get i18n(): any {
    return I18N_DWFE_ACTION_CLEAR;
  }
}
