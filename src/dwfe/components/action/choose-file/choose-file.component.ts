import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {DwfeGlobals} from '../../../globals';
import {DwfeAbstractEditableControl} from '../../../classes/DwfeAbstractEditableControl';

import {I18N_DWFE_CHOOSE_FILE} from './i18n';

@Component({
  selector: 'dwfe-action-choose-file',
  templateUrl: './choose-file.component.html',
  styleUrls: ['./choose-file.component.scss']
})
export class DwfeActionChooseFileComponent extends DwfeAbstractEditableControl implements OnInit {

  @Input() maxFileSizeInBytes: number;
  @Input() validExtentions: string[];

  @Output() chosenFile = new EventEmitter<File>();
  @Output() takeClick = new EventEmitter<boolean>();

  file: File;
  fileName = '';
  formatBytes = DwfeGlobals.formatBytes;

  constructor(public app: AppService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit(); // here it is just in case
  }

  fileSelected(files: File[]) {
    if (files.length > 0) {
      this.file = files[0];
      this.fileName = this.file.name;
      if (this.isFileValid()) {
        this.chosenFile.emit(this.file);
      } else {
        this.sendNullFile();
      }
    } else {
      this.resetChoosedFile();
    }
  }

  resetChoosedFile() {
    this.file = null;
    this.fileName = '';
    this.sendNullFile();
  }

  sendNullFile(): void {
    this.chosenFile.emit(null);
  }

  needCheckFileSize(): boolean {
    return !!this.maxFileSizeInBytes;
  }

  needCheckFileExtention(): boolean {
    return this.validExtentions && this.validExtentions.length > 0;
  }

  isFileValid(): boolean {
    return this.isFileSizeValid() && this.isFileExtentionValid();
  }

  isFileSizeValid(): boolean {
    if (this.needCheckFileSize()) {
      return this.file && this.file.size <= this.maxFileSizeInBytes;
    } else {
      return true;
    }
  }

  isFileExtentionValid(): boolean {
    if (this.needCheckFileExtention()) {
      if (this.file) {
        const fileExtention = DwfeGlobals.extractFileExtention(this.fileName);
        return this.validExtentions.includes(fileExtention);
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  getTextValidExtentions(): string {
    let result = '';
    if (this.needCheckFileExtention()) {
      const stopDelimiter = this.validExtentions.length - 1;
      this.validExtentions.forEach((extention, index) => {
        result += extention + (index < stopDelimiter ? ', ' : '');
      });
    }
    return result;
  }

  get i18n(): any {
    return I18N_DWFE_CHOOSE_FILE;
  }
}
