import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {MatButtonModule} from '@angular/material';

import {DwfeActionChooseFileComponent} from './choose-file.component';

@NgModule({
  declarations: [
    DwfeActionChooseFileComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    MatButtonModule,
  ],
  exports: [
    DwfeActionChooseFileComponent,
  ],
})
export class DwfeActionChooseFileModule {
}
