export const I18N_DWFE_CHOOSE_FILE = {

  choose_file: {
    EN: 'Choose file',
    RU: 'Выберите файл',
  },
  max_file_size: {
    EN: 'Maximum file size',
    RU: 'Максимальный размер файла',
  },
  valid_extentions: {
    EN: 'Valid file extentions',
    RU: 'Доступные типы файлов',
  },
};
