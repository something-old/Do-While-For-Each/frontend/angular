import {Component, EventEmitter, Input, Output} from '@angular/core';

import {AppService} from '@app/services/app.service';

import {T7E_DWFE_ACTION_OK_CANCEL} from './i18n';

@Component({
  selector: 'dwfe-action-ok-cancel',
  templateUrl: './action-ok-cancel.component.html',
  styleUrls: ['./action-ok-cancel.component.scss']
})
export class DwfeActionOkCancelComponent {

  @Input() isDisabled = false;

  @Input() showErrorMessage = false;
  @Input() showSuccessMessage = false;

  @Input() errorMessage: string;
  @Input() successMessage: string;

  @Input() LabelOk = 'Ok';

  @Output() takeOk = new EventEmitter<boolean>();
  @Output() takeCancel = new EventEmitter<boolean>();

  constructor(public app: AppService) {
  }

  get i18n(): any {
    return T7E_DWFE_ACTION_OK_CANCEL;
  }
}
