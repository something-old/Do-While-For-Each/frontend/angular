import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MatButtonModule} from '@angular/material';

import {DwfeAlertModule} from '@dwfe/components/alert/alert.module';

import {DwfeActionOkCancelComponent} from './action-ok-cancel.component';

@NgModule({
  declarations: [
    DwfeActionOkCancelComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    DwfeAlertModule,
  ],
  exports: [
    DwfeActionOkCancelComponent,
  ],
})
export class DwfeActionOkCancelModule {
}
