import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeSpinnerSharkFinCircleModule} from '../spinner/shark-fin-circle/shark-fin-circle.module';
import {DwfeOverlayComponent} from './overlay.component';

@NgModule({
  declarations: [
    DwfeOverlayComponent,
  ],
  imports: [
    CommonModule,

    DwfeSpinnerSharkFinCircleModule,
  ],
  exports: [
    DwfeOverlayComponent,
  ],
})
export class DwfeOverlayModule {
}
