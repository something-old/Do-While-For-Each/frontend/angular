import {Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'dwfe-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DwfeOverlayComponent {

  @Input() showSpinner = true;
}
