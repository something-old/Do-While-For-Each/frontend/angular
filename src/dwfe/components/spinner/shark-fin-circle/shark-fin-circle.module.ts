import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DwfeSpinnerSharkFinCircleComponent} from '@dwfe/components/spinner/shark-fin-circle/shark-fin-circle.component';

@NgModule({
  declarations: [
    DwfeSpinnerSharkFinCircleComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DwfeSpinnerSharkFinCircleComponent,
  ],
})
export class DwfeSpinnerSharkFinCircleModule {
}
