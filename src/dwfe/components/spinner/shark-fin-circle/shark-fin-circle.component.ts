import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-shark-fin-circle',
  templateUrl: './shark-fin-circle.component.html',
  styleUrls: ['./shark-fin-circle.component.scss']
})
export class DwfeSpinnerSharkFinCircleComponent {
}
