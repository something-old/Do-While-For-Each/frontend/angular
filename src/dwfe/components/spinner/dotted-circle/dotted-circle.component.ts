import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-dotted-circle',
  templateUrl: './dotted-circle.component.html',
  styleUrls: ['./dotted-circle.component.scss']
})
export class DwfeSpinnerDottedCircleComponent {
}
