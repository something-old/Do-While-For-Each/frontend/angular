import {NgModule} from '@angular/core';

import {DwfeSpinnerDottedCircleComponent} from './dotted-circle.component';

@NgModule({
  declarations: [
    DwfeSpinnerDottedCircleComponent,
  ],
  imports: [],
  exports: [
    DwfeSpinnerDottedCircleComponent,
  ],
})
export class DwfeSpinnerDottedCircleModule {
}
