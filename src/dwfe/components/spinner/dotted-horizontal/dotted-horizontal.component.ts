import {Component} from '@angular/core';

@Component({
  selector: 'dwfe-spinner-dotted-horizontal',
  templateUrl: './dotted-horizontal.component.html',
  styleUrls: ['./dotted-horizontal.component.scss']
})
export class DwfeSpinnerDottedHorizontalComponent {
}
