import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DwfeActionCloseModule} from '../../action/close/action-close.module';

import {DwfeDialogSimpleComponent} from './dialog-simple.component';

@NgModule({
  declarations: [
    DwfeDialogSimpleComponent,
  ],
  imports: [
    CommonModule,
    DwfeActionCloseModule,
  ],
  exports: [
    DwfeDialogSimpleComponent,
  ]
})
export class DwfeDialogSimpleModule {
}
