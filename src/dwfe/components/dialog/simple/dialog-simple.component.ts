import {Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';

import {DwfeGlobals} from '@dwfe/globals';

@Component({
  selector: 'dwfe-dialog-simple',
  templateUrl: './dialog-simple.component.html',
  styleUrls: ['./dialog-simple.component.scss']
})
export class DwfeDialogSimpleComponent implements OnInit, OnDestroy {

  @Input() showCloseBtn = false;
  @Output() closeAction = new EventEmitter<boolean>();

  bodyClassList = document.getElementsByTagName('body')[0].classList;
  overflowClassName = 'dwfe-modal-open';
  isAddedBodyOverlayHidden = false;

  ngOnInit(): void {
    if (!this.isBodyAlreadyOverlayHidden) {
      this.bodyClassList.add(this.overflowClassName);
      this.isAddedBodyOverlayHidden = true;
    }
  }

  get isBodyAlreadyOverlayHidden(): boolean {
    return this.bodyClassList.contains(this.overflowClassName);
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(evt: KeyboardEvent) {

    if (this.showCloseBtn
      && DwfeGlobals.isEscapeKey(evt)) {
      this.close();
    }
  }

  close() {
    this.closeAction.emit(true);
  }

  ngOnDestroy(): void {
    if (this.isAddedBodyOverlayHidden) {
      this.bodyClassList.remove(this.overflowClassName);
    }
  }
}
