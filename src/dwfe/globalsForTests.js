//
// VARIABLES
//

const testUserName = 'test1@dwfe.ru';
const testPassword = 'test11';

//
// FUNCTIONS
//
async function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// ===

module.exports.testUserName = testUserName;
module.exports.testPassword = testPassword;

module.exports.delay = delay;
