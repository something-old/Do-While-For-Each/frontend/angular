import {AfterContentInit, ContentChildren, Directive, OnDestroy, OnInit, QueryList} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MatTabGroup} from '@angular/material';

import {Observable, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';

import {DwfeRoutedTabDirective} from './routed-tab.directive';

@Directive({
  selector: '[routedTabs]'
})
export class DwfeRoutedTabsDirective implements OnInit, AfterContentInit, OnDestroy {

  // Based on the zerohouse work:
  // https://github.com/zerohouse/router-tab
  //
  // because:
  // https://github.com/angular/material2/issues/2177

  @ContentChildren(DwfeRoutedTabDirective) routedTabs: QueryList<DwfeRoutedTabDirective>;

  subjLatchForUnsubscribe = new Subject();

  constructor(public matTabGroup: MatTabGroup, public router: Router) {
  }

  get latchForUnsubscribe$(): Observable<any> {
    return this.subjLatchForUnsubscribe.asObservable();
  }

  ngOnDestroy(): void {
    this.subjLatchForUnsubscribe.next();
  }

  ngOnInit(): void {

    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd),
        takeUntil(this.latchForUnsubscribe$))
      .subscribe(
        () => this.setSelectedIndex()
      );

    this.matTabGroup.selectedTabChange
      .pipe(
        takeUntil(this.latchForUnsubscribe$)
      )
      .subscribe(() => {
          this.routedTabs.find(routedTabi => {
            const urlTree = routedTabi.routerLink.urlTree;
            if (routedTabi.matTab.isActive && !this.router.isActive(urlTree, false)) {
              this.router.navigateByUrl(urlTree);
              return true;
            }
          });
        }
      );
  }

  ngAfterContentInit(): void {

    this.setSelectedIndex(); // because 'router.events -> NavigationEnd' has already occurred
                             // BEFORE init nested tabs, of course, if nested tabs are present
  }

  setSelectedIndex() {
    this.routedTabs.find((routedTabi, i) => {
      if (this.router.isActive(routedTabi.routerLink.urlTree, false)) {
        this.matTabGroup.selectedIndex = i;
        return true;
      }
    });
  }
}
